# -*- coding: utf-8 -*-
class Calificacion:

    def __init__(self, nota1, nota2, nota3, nota4):
        self.__nota1 = nota1
        self.__nota2 = nota2
        self.__nota3 = nota3
        self.__nota4 = nota4

    def __promediar(self):
        promedio = (self.__nota1 + self.__nota2 + self.__nota3 + self.__nota4)/4.0
        return promedio

    def get_mostrar_notafinal(self):
        if self.__promediar() >=0 and self.__promediar() <=59.9:
            return "Tu nota es una E"
        elif self.__promediar() >=60 and self.__promediar() <=69.9:
            return "Tu nota es una D"
        elif self.__promediar() >=70 and self.__promediar() <=79.9:
            return "Tu nota es una C"
        elif self.__promediar() >=80 and self.__promediar() <=89.9:
            return "Tu nota es una B"
        else:
            return "Tu nota es una A"



obj = Calificacion(99, 70, 90, 87)
# print obj.__promediar()
print obj.get_mostrar_notafinal()
